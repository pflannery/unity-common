﻿using Common.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Common.Editor.PropertyDrawers
{

    [CustomPropertyDrawer(typeof(ReadOnlyInEditorAttribute))]
    public class ReadOnlyInEditorPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect rect, SerializedProperty prop, GUIContent label)
        {
            var readOnlyAttr = (ReadOnlyInEditorAttribute)attribute;
            bool wasEnabled = GUI.enabled;
            GUI.enabled = false;
            EditorGUI.PropertyField(rect, prop, readOnlyAttr.IncludeChildren);
            GUI.enabled = wasEnabled;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            // use the default property height, which takes into account the expanded state
            return EditorGUI.GetPropertyHeight(property);
        }

    }

}
