﻿namespace Common.Editor.PropertyDrawers
{
    using Common.PropertyAttributes;
    using UnityEditor;
    using UnityEngine;

    [CustomPropertyDrawer(typeof(ShowInEditorWhenAttribute))]
    public class ShowInEditorWhenPropertyDrawer : PropertyDrawer
    {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var attr = (ShowInEditorWhenAttribute)attribute;
            var enabled = GetShowInEditorWhenAttributeResult(attr, property);

            if (enabled)
            {
                if (attr.indent) EditorGUI.indentLevel++;
                EditorGUI.PropertyField(position, property, label, true);
                if (attr.indent) EditorGUI.indentLevel--;
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var attr = (ShowInEditorWhenAttribute)attribute;
            var enabled = GetShowInEditorWhenAttributeResult(attr, property);

            if (enabled)
            {
                return EditorGUI.GetPropertyHeight(property, label);
            }

            //We want to undo the spacing added before and after the property
            return -EditorGUIUtility.standardVerticalSpacing;
        }

        bool GetShowInEditorWhenAttributeResult(ShowInEditorWhenAttribute attr, SerializedProperty property)
        {
            SerializedProperty sourcePropertyValue = null;

            //Get the full relative property path of the sourcefield so we can have nested hiding.Use old method when dealing with arrays
            if (!property.isArray)
            {
                var propertyPath = property.propertyPath; //returns the property path of the property we want to apply the attribute to
                var conditionPath = propertyPath.Replace(property.name, attr.propertyName); //changes the path to the conditionalsource property path
                sourcePropertyValue = property.serializedObject.FindProperty(conditionPath);

                //if the find failed->fall back to the old system
                if (sourcePropertyValue == null)
                {
                    //original implementation (doens't work with nested serializedObjects)
                    sourcePropertyValue = property.serializedObject.FindProperty(attr.propertyName);
                }
            }
            else
            {
                //original implementation (doens't work with nested serializedObjects)
                sourcePropertyValue = property.serializedObject.FindProperty(attr.propertyName);
            }

            if (sourcePropertyValue != null)
            {
                return CheckPropertyType(attr, sourcePropertyValue);
            }

            return true;
        }

        bool CheckPropertyType(ShowInEditorWhenAttribute attr, SerializedProperty sourcePropertyValue)
        {
            //Note: add others for custom handling if desired
            switch (sourcePropertyValue.propertyType)
            {
                case SerializedPropertyType.Boolean:
                    return sourcePropertyValue.boolValue;
                case SerializedPropertyType.Enum:
                    return sourcePropertyValue.enumValueIndex == attr.enumIndex;
                default:
                    Debug.LogError("Data type of the property used for conditional hiding [" + sourcePropertyValue.propertyType + "] is currently not supported");
                    return true;
            }
        }
    }
}
