﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Common.Helpers
{
    public static class DebugHelper
    {
        public static void Log(params object[] messages)
        {
            var sb = new StringBuilder();

            foreach (var msg in messages)
            {
                sb.Append(msg == null ? "Null" : msg.ToString());
                sb.Append(", ");
            }

            Debug.Log(sb.ToString());
        }

        public static void DrawCircle(Vector3 position, float radius, Color color)
        {
            var points = DebugHelper.GetCirclePoints(position, radius);
            for (int i = 0; i < points.Count; i++)
            {
                Debug.DrawLine(points[i], points[(i + 1) % points.Count], color);
            }
        }

        public static List<Vector3> GetCirclePoints(Vector3 position, float radius)
        {
            var sides = 16;
            var angleInc = (Mathf.PI * 2) / sides;
            var angle = 0f;
            var counter = sides;
            var points = new List<Vector3>();

            for (; counter > 0; counter--)
            {
                float x = position.x + (radius * Mathf.Sin(angle));
                float y = position.y + (radius * Mathf.Cos(angle));
                float z = position.z;
                points.Add(new Vector3(x, y, z));
                angle += angleInc;
            }

            return points;
        }

        public static void DrawAxesNormals(Vector3 position, Vector3 forward, Vector3 side, Vector3 up, float lineLength = 10f)
        {
            Debug.DrawLine(position, position + (up * lineLength), Color.blue);
            Debug.DrawLine(position, position + (side * lineLength), Color.red);
            Debug.DrawLine(position, position + (forward * lineLength), Color.green);
        }
    }
}