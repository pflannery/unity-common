﻿using Common.Vector3Extensions;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Common.Helpers
{
    public static class LineHelper
    {

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 LineProjection(Vector3 point, Vector3 start, Vector3 end)
        {
            var a = point - start;
            var b = end - start;
            var perp = a.Project(b.normalized);
            return start + perp;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 LineProjectionClamped(Vector3 point, Vector3 start, Vector3 end)
        {
            return LineProjection(point, start, end).Clamp(start, end);
        }

    }
}