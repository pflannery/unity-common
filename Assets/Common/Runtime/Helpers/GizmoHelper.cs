﻿using UnityEngine;

namespace Common.Helpers
{
    public static class GizmoHelper
    {
        public static void DrawCircle(Vector3 position, float radius)
        {
            var points = DebugHelper.GetCirclePoints(position, radius);
            for (int i = 0; i < points.Count; i++)
            {
                Gizmos.DrawLine(points[i], points[(i + 1) % points.Count]);
            }
        }
    }
}