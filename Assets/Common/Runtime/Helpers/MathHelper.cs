﻿using UnityEngine;

namespace Common.Helpers
{
    public static class MathHelper
    {
        public const float TWO_PI = Mathf.PI * 2;

        public static float Wrap(float value, float max)
        {
            var wv = value % max;
            if ((wv < 0 && max > 0) || (wv > 0 && max < 0)) wv += max;
            return wv;
        }

        public static float DeltaAngle(float from, float to, float max)
        {
            var am = to - from;
            var bm = am + max;
            var cm = am - max;

            var a = Mathf.Abs(am);
            var b = Mathf.Abs(bm);
            var c = Mathf.Abs(cm);

            var dir = Mathf.Min(a, b, c);

            var smallest = am;
            if (dir == b)
                smallest = bm;
            else if (dir == c)
                smallest = cm;

            return smallest;
        }

    }
}