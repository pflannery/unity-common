﻿namespace Common.PropertyAttributes
{
    using System;
    using UnityEngine;

    [AttributeUsage(AttributeTargets.Field, Inherited = true)]
    public class ShowInEditorWhenAttribute : PropertyAttribute
    {
        public string propertyName;
        public int enumIndex;
        public bool indent = true;

        public ShowInEditorWhenAttribute(string propertyName)
        {
            this.propertyName = propertyName;
        }

        public ShowInEditorWhenAttribute(string propertyName, int enumIndex)
        {
            this.propertyName = propertyName;
            this.enumIndex = enumIndex;
        }

    }

}
