﻿using System;
using UnityEngine;

namespace Common.PropertyAttributes
{

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = true)]
    public class ReadOnlyInEditorAttribute : PropertyAttribute
    {
        public bool IncludeChildren;

        public ReadOnlyInEditorAttribute() { }

        public ReadOnlyInEditorAttribute(bool IncludeChildren)
        {
            this.IncludeChildren = IncludeChildren;
        }
    }

}
