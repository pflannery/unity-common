﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Common.ListExtensions
{
    public static class ListExtensions
    {
        /// <summary>
        /// Performs a modulo operation using the index and list length. i.e. n = (i + 1) % len
        /// Supports negative numbers. -1 is len-1, -2 is len-2 ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int CyclicIndex<T>(this IList<T> list, int index)
        {
            int i = index % list.Count;
            if ((i < 0 && list.Count > 0) || (i > 0 && list.Count < 0)) i += list.Count;
            return i;
        }

        /// <summary>
        /// Uses the CyclicIndex method when performing an item lookup within the list. See CyclicIndex for more information.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T CyclicValue<T>(this IList<T> list, int index)
        {
            return list[list.CyclicIndex(index)];
        }

        /// <summary>
        /// Returns the added item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T Append<T>(this IList<T> list, T item)
        {
            list.Add(item);
            return item;
        }
    }
}