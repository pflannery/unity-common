﻿using System.Runtime.CompilerServices;
using UnityEngine;

namespace Common.Vector3Extensions
{

    public static class Vector3Extensions
    {

        /// <summary>
        /// The vector projection of a vector a onto a nonzero vector b (also known as the vector component or vector resolution of a in the direction of b)
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b̂"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 Project(this Vector3 a, Vector3 b)
        {
            // https://en.wikipedia.org/wiki/Vector_projection
            // a1 = (a·b̂)b̂:
            return Vector3.Dot(a, b) * b;
        }

        /// <summary>
        /// Clamps a vector between two other vectors and returns the result.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 Clamp(this Vector3 point, Vector3 start, Vector3 end)
        {
            var toStart = (point - start).sqrMagnitude;
            var toEnd = (point - end).sqrMagnitude;
            var segment = (start - end).sqrMagnitude;

            if (toStart > segment || toEnd > segment)
            {
                return toStart > toEnd ? end : start;
            }

            return point;
        }


    }

}