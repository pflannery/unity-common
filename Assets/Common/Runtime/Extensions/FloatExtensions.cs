﻿using System;
using System.Runtime.CompilerServices;

namespace Common.FloatExtensions
{

    public static class FloatExtensions
    {

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Round(this float value, int decimals = 0) => (float)Math.Round(value, decimals);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Floor(this float value) => (float)Math.Floor(value);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Ceiling(this float value) => (float)Math.Ceiling(value);

    }

}
