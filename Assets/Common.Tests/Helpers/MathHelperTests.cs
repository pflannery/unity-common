﻿using Common.Helpers;
using NUnit.Framework;
using UnityEngine;

namespace Helpers
{

    public class MathHelperTests
    {

        #region Wrap

        [Test, Category("MathHelper")]
        public void Wrap_Zero()
        {
            var max = 360;
            Assert.AreEqual(0, MathHelper.Wrap(-max, max));
            Assert.AreEqual(0, MathHelper.Wrap(0, max));
            Assert.AreEqual(0, MathHelper.Wrap(max, max));
        }

        [Test, Category("MathHelper")]
        public void Wrap_Positive_Numbers()
        {
            var max = 360;
            var e = 0;
            for (int i = 0; i < max; i++, e++)
            {
                Assert.AreEqual(e, MathHelper.Wrap(i, max));
            }

            e = 0;
            for (int i = max; i < (max * 2); i++, e++)
            {
                Assert.AreEqual(e, MathHelper.Wrap(i, max));
            }
        }

        [Test, Category("MathHelper")]
        public void Wrap_Negative_Numbers()
        {
            var max = 360;
            var e = 0;
            for (int i = -max; i < 0; i++, e++)
            {
                Assert.AreEqual(e, MathHelper.Wrap(i, max));
            }

            e = 0;
            for (int i = -(max * 2); i < -max; i++, e++)
            {
                Assert.AreEqual(e, MathHelper.Wrap(i, max));
            }
        }

        #endregion

        #region DeltaAngle

        [Test, Category("MathHelper")]
        public void DeltaAngle_Inside_Circle()
        {
            // in bounds tets
            Assert.AreEqual(-1f, MathHelper.DeltaAngle(2, 1, MathHelper.TWO_PI));
            Assert.AreEqual(1f, MathHelper.DeltaAngle(1, 2, MathHelper.TWO_PI));
        }

        [Test, Category("MathHelper")]
        public void DeltaAngle_Two_PI_Boundary()
        {
            // out of bound tests
            Assert.AreEqual(-0.4831853f, MathHelper.DeltaAngle(0.2f, 6, MathHelper.TWO_PI));
            Assert.AreEqual(0.4831853f, MathHelper.DeltaAngle(6, 0.2f, MathHelper.TWO_PI));
        }

        [Test, Category("MathHelper")]
        public void DeltaAngle_PI()
        {
            // PI tests
            Assert.AreEqual(Mathf.PI, MathHelper.DeltaAngle(Mathf.PI, 0, MathHelper.TWO_PI));
            Assert.AreEqual(-Mathf.PI, MathHelper.DeltaAngle(-Mathf.PI, 0, MathHelper.TWO_PI));
        }

        #endregion

    }


}