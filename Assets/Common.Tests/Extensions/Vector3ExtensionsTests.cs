﻿using Common.Vector3Extensions;
using NUnit.Framework;
using UnityEngine;

namespace Extensions
{

    public class Vector3ExtensionsTests
    {

        #region Project

        [Test, Category("Vector3ExtensionsTests"), Category("Vector3.Project")]
        public void Project_PositiveChecks()
        {
            Vector3 a, b, actual;

            a = new Vector3(0, 10);
            b = new Vector3(10, 0);
            actual = a.Project(b.normalized);

            Assert.AreEqual(0f, actual.x);
            Assert.AreEqual(0f, actual.y);

            a = new Vector3(0, 10);
            b = new Vector3(10, 10);
            actual = a.Project(b.normalized);

            Assert.AreEqual(5f, actual.x);
            Assert.AreEqual(5f, actual.y);

            a = new Vector3(0, 10);
            b = new Vector3(0, 10);
            actual = a.Project(b.normalized);

            Assert.AreEqual(0f, actual.x);
            Assert.AreEqual(10f, actual.y);

            a = new Vector3(0, 10);
            b = new Vector3(0, 0);
            actual = a.Project(b.normalized);

            Assert.AreEqual(0f, actual.x);
            Assert.AreEqual(0f, actual.y);
        }

        [Test, Category("Vector3ExtensionsTests"), Category("Vector3.Project")]
        public void Project_NegativeChecks()
        {
            Vector3 a, b, actual;

            a = new Vector3(0, -10);
            b = new Vector3(-10, 0);
            actual = a.Project(b.normalized);

            Assert.AreEqual(0f, actual.x);
            Assert.AreEqual(0f, actual.y);

            a = new Vector3(0, -10);
            b = new Vector3(-10, -10);
            actual = a.Project(b.normalized);

            Assert.AreEqual(-5f, actual.x);
            Assert.AreEqual(-5f, actual.y);

            a = new Vector3(0, -10);
            b = new Vector3(0, -10);
            actual = a.Project(b.normalized);

            Assert.AreEqual(0f, actual.x);
            Assert.AreEqual(-10f, actual.y);

            a = new Vector3(0, -10);
            b = new Vector3(0, 0);
            actual = a.Project(b.normalized);

            Assert.AreEqual(0f, actual.x);
            Assert.AreEqual(0f, actual.y);
        }

        #endregion

        #region Clamp

        [Test, Category("Vector3ExtensionsTests"), Category("Vector3.Clamp")]
        public void Clamp_GreaterValues()
        {
            Vector3 min, max, actual;

            // x test
            min = new Vector3(0, 0, 0);
            max = new Vector3(10, 0, 0);

            actual = new Vector3(11, 0, 0).Clamp(min, max);

            Assert.AreEqual(10f, actual.x);

            // y test
            min = new Vector3(0, 0, 0);
            max = new Vector3(0, 10, 0);

            actual = new Vector3(0, 11, 0).Clamp(min, max);

            Assert.AreEqual(10f, actual.y);

            // z test
            min = new Vector3(0, 0, 0);
            max = new Vector3(0, 0, 10);

            actual = new Vector3(0, 0, 11).Clamp(min, max);

            // xyz test
            min = new Vector3(0, 0, 0);
            max = new Vector3(10, 10, 10);

            actual = new Vector3(11, 11, 11).Clamp(min, max);

            Assert.AreEqual(10f, actual.x);
            Assert.AreEqual(10f, actual.y);
            Assert.AreEqual(10f, actual.z);
        }

        [Test, Category("Vector3ExtensionsTests"), Category("Vector3.Clamp")]
        public void Clamp_LesserValues()
        {
            Vector3 min, max, actual;

            // x test
            min = new Vector3(0, 0, 0);
            max = new Vector3(10, 0, 0);

            actual = new Vector3(-1, 0, 0).Clamp(min, max);

            Assert.AreEqual(0f, actual.x);

            // y test
            min = new Vector3(0, 0, 0);
            max = new Vector3(0, 10, 0);

            actual = new Vector3(0, -1, 0).Clamp(min, max);

            Assert.AreEqual(0f, actual.y);

            // z test
            min = new Vector3(0, 0, 0);
            max = new Vector3(0, 0, 10);

            actual = new Vector3(0, 0, -1).Clamp(min, max);

            Assert.AreEqual(0f, actual.z);

            // xyz test
            min = new Vector3(0, 0, 0);
            max = new Vector3(10, 10, 10);

            actual = new Vector3(-1, -1, -1).Clamp(min, max);

            Assert.AreEqual(0f, actual.x);
            Assert.AreEqual(0f, actual.y);
            Assert.AreEqual(0f, actual.z);
        }

        #endregion

    }

}
