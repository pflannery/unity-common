﻿using Common.ListExtensions;
using NUnit.Framework;

namespace Extensions
{

    public class ListExtensionsTests
    {

        [Test, Category("ListExtensions"), Category("IList.CyclicIndex")]
        public void Wrap_Zero()
        {
            var count = 360;
            var testList = new int[count];
            Assert.AreEqual(0, testList.CyclicIndex(-count));
            Assert.AreEqual(0, testList.CyclicIndex(0));
            Assert.AreEqual(0, testList.CyclicIndex(count));
        }

        [Test, Category("ListExtensions"), Category("IList.CyclicIndex")]
        public void CyclicIndex_Positive_Indexes()
        {
            var count = 360;
            var testList = new int[count];
            var e = 0;
            for (int i = 0; i < count; i++, e++)
            {
                Assert.AreEqual(e, testList.CyclicIndex(i));
            }

            e = 0;
            for (int i = count; i < (count * 2); i++, e++)
            {
                Assert.AreEqual(e, testList.CyclicIndex(i));
            }
        }

        [Test, Category("ListExtensions"), Category("IList.CyclicIndex")]
        public void CyclicIndex_Negative_Indexes()
        {
            var count = 360;
            var testList = new int[count];

            var e = 0;
            for (int i = -count; i < 0; i++, e++)
            {
                Assert.AreEqual(e, testList.CyclicIndex(i));
            }

            e = 0;
            for (int i = -(count * 2); i < -count; i++, e++)
            {
                Assert.AreEqual(e, testList.CyclicIndex(i));
            }
        }

    }

}
