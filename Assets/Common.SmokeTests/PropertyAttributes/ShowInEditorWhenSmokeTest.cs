﻿using Common.PropertyAttributes;
using UnityEngine;

namespace Common.SmokeTests
{

    public class ShowInEditorWhenSmokeTest : MonoBehaviour
    {

        public enum YesNo
        {
            Yes,
            No
        }

        public YesNo TestEnum;

        [ShowInEditorWhen("TestEnum", 0, indent = false)] public float ShouldBeVisibleWhenYes = 123f;

        [ShowInEditorWhen("TestEnum", 1, indent = false)] public string ShouldBeVisibleWhenNo = "Enum Test: Can you see me";

        public bool TestBool;

        [ShowInEditorWhen("TestBool")] public string ShouldBeVisibleWhenTrue = "Bool Test: Can you see me";
    }

}