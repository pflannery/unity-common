﻿using Common.PropertyAttributes;
using UnityEngine;

namespace Common.SmokeTests
{

    public class ReadOnlyInEditorSmokeTest : MonoBehaviour
    {

        [ReadOnlyInEditor] public float ShouldBeReadOnly = 123f;

    }

}