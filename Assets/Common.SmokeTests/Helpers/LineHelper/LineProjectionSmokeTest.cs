﻿using Common.Helpers;
using UnityEngine;

namespace Common.SmokeTests
{

    public class LineProjectionSmokeTest : MonoBehaviour
    {

        public Transform Point;
        public Transform Start;
        public Transform End;

        Vector3 lp;

        float a = 0f;
        float aStep = .05f;
        float r = 10f;

        void Update()
        {

            Point.position = new Vector3(
                r * Mathf.Sin(a),
                0,
                r * Mathf.Cos(a)
            );

            a += aStep;

            lp = LineHelper.LineProjection(Point.position, Start.position, End.position);

        }

        void OnDrawGizmosSelected()
        {

            Gizmos.color = Color.white;
            Gizmos.DrawLine(Start.position, End.position);

            Gizmos.color = Color.white;
            Gizmos.DrawWireCube(Point.position, Vector3.one * 1f);

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(lp, Vector3.one * 1f);

        }


    }

}