# Common things I use in my unity projects

Contains some useful extensions and helpers

## Requirements

> The unity scripting runtime must be set to ".NET 4.x Equivalent"

> You must setup C# 7.x compilation in Unity 2018.2 projects by

[Instructions on how to setup C# 7.x Unity 2018.2](https://gist.github.com/pflannery/9a03c174df22098f6904ead5230cfc94)
